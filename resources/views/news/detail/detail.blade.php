@extends('layouts.master')
@section('styles')
      <link rel="stylesheet" href="{{ asset('css/desgin_competition.css') }}">
@endsection
@section('content')
<?php $langNetis = \App::getLocale(); ?>
@if(isset($news))
<div class="wrap">
    <div class="content-main" id="content-main">
        <div class="container container-body">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="row">
                        <div class="meta-nav backshare col-md-12">
                            <div class="col-xs-3 col-sm-6">
                                <a href="javascript:history.go(-1)" class="upper sub"> <span>&lt; {{trans('products.back')}}</span> </a>
                            </div>
                        </div>
                    </div>
                    <!--/UdmComment-->
                    <div class="row contentrow">
                        @if($langNetis == 'vi')
                        <div class="component-headline">
                            <div class="headline_title">
                                <h2>{{$news->title_vi}}</h2></div>
                            <div class="headline_text">
                                <p><b>{{$news->recommend_vi}}</b></p>
                            </div>
                        </div>
             <!--            <div class="component-promo featured col-xs-12 col-sm-6">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="image-container">

                                        <img alt="SGB_DesignContest2018_Kostroma_A5_PRINT-1.jpg" class=" img-responsive lazyloaded" src="/img/news/detail/SGB_DesignContest2018_Kostroma_A5_PRINT-1.jpg"> </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="component-promo-text">
                                        <p class="slide-meta hidden"> </p>
                                        <h4 class="title"></h4>
                                        <div>
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <style>
                        	.component-promo .component-promo-text{
                        		text-align: center;
                        	}
                        </style>
                        <div class="component-promo featured col-xs-12 col-sm-12">
                            <div class="row">
                                <div class="col-xs-12"> </div>
                                <div class="col-xs-12">
                                    <div class="component-promo-text">
                                        <!-- Meta -->
                                        <p class="slide-meta hidden"> </p>
                                        <!-- Promo text -->
                                        <h4 class="title"></h4>
                                        <div>
                                            <p>{!!$news->content_vi!!}</p>
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @else
                              <div class="component-headline">
                            <div class="headline_title">
                                <h2>{{$news->title_en}}</h2></div>
                            <div class="headline_text">
                                <p><b>{{$news->recommend_en}}</b></p>
                            </div>
                        </div>
                        <div class="component-promo featured col-xs-12 col-sm-6">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="image-container">

                                        <img alt="SGB_DesignContest2018_Kostroma_A5_PRINT-1.jpg" class=" img-responsive lazyloaded" src="/img/news/detail/SGB_DesignContest2018_Kostroma_A5_PRINT-1.jpg"> </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="component-promo-text">
                                        <p class="slide-meta hidden"> </p>
                                        <h4 class="title"></h4>
                                        <div>
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="component-promo featured col-xs-12 col-sm-6">
                            <div class="row">
                                <div class="col-xs-12"> </div>
                                <div class="col-xs-12">
                                    <div class="component-promo-text">
                                        <!-- Meta -->
                                        <p class="slide-meta hidden"> </p>
                                        <!-- Promo text -->
                                        <h4 class="title"></h4>
                                        <div>
                                            <p>The jewelry industry thrives on fresh and innovative new ideas, so that is why a Design Competition by Swarovski is poised to invigorate the Russian jewelry and watches landscape with brilliant concepts using the Swarovski Genuine Gemstones and Created Stones product assortment. The intention is to inspire modern, authentic Russian designs that will captivate the market.&nbsp;&nbsp;</p>
                                            <p>Judges will look for an explosion of colors and styles that reflect the theme “Tropical Paradise” in designs that are both visually exciting and distinctly fashion-forward in their presentation. Contestants are invited to draw inspiration from the world’s rainforests and jungles, and from exotic flowers and fruits.&nbsp;</p>
                                            <p>The competition is open to designers, manufacturers and all those who aspire to be creative using innovative materials and styles. Candidates must come from Russia or a Russian-speaking country, and be 18-years-old at the time of submitting their design. Finalists must also be able to attend the Junwex fair at the end of September in Kostroma, Russia, where Swarovski will announce the competition winner at a special dinner held at the end of the fair. The winner of the overall best design receives a trip to the Swarovski Headquarters in Wattens, Austria, and the best designs will have the possibility to be published in selected Swarovski marketing publications. A professional jury, made up of international experts and designers will determine the winning designs.&nbsp; Find out all you need to know about the competition <a target="_blank" href="http://www.crystalit.ru/konkurs-swarovski-gemstones-tropicheskij-raj-dlya-yuvelirov-i-dizajnerov2018" class="" data-gentics-gcn-url="http://www.crystalit.ru/konkurs-swarovski-gemstones-tropicheskij-raj-dlya-yuvelirov-i-dizajnerov2018">here</a>.&nbsp;</p>
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endsection