@extends('layouts.master')
@section('styles')  
      <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" href="../css/dancings_stones.css">
@endsection
@section('content')
<?php $langNetis = \App::getLocale(); ?>
<div class="wrap">

    <div class="content-main" id="content-main">

        <!-- Page Content
        ================================================== -->
        <div class="container container-body">
            <div class="row">

             <!--    <div class="col-xs-12 col-md-2">

                    <div class="component-sidebar-menu component-menu-collapse" id="component-sidebar-menu">
                        <div class="btn-group dropdown">
                            <ul class="dropdown-menu " role="menu" aria-labelledby="component-sidebar-menu">

                                <li class="subnav" role="menuitem">
                                    <h5 class="title ">						<a href="/news/News.en.html" class="">						    News						</a>					    </h5>
                                    <ul class="dropdown-menu">

                                        <li role="menuitem" class="folder"> <a href="/news/Industry_News.en.html" class="">						    Industry News						</a> </li>

                                        <li role="menuitem" class="folder"> <a href="/news/Productstories.en.html" class="">						    Product Stories						</a> </li>

                                        <li role="menuitem" class="folder"> <a href="/news/Innovation_Lab_startpage.en.html" class="">						    Innovation Lab						</a> </li>

                                        <li role="menuitem" class="folder"> <a href="/news/collaborations.en.html" class="">						    Collaborations						</a> </li>

                                        <li role="menuitem" class="folder"> <a href="/news/Inspirations.en.html" class="">						    Inspirations						</a> </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div> -->

                <div class="col-xs-12 col-md-12">
                    <div class="row">
                        <div class="meta-nav backshare col-md-12">
                            <div class="col-xs-3 col-sm-6">
                                <a href="javascript:history.go(-1)" class="upper sub"> <span>&lt; {{trans('products.back')}}</span> </a>
                            </div>
<!--                             <div class="col-xs-9 col-sm-6 text-right">

                          
                                <div class="component-addthis-toolbox addthis_default_style addthis_32x32_style" role="menuitem">
                                    <a class="addthis_button_facebook ir  ir-social ir-facebook" href="http://www.facebook.com/share.php?u=" onclick="return facebook_share(520, 350)" target="_blank" title="Share This on Facebook"></a>
                                    <a class="addthis_button_twitter ir  ir-social ir-twitter" href="http://twitter.com/intent/tweet?status=%20+-+%20" onclick="return twitter_share(520, 350)" target="_blank" title="Share This on Twitter"></a>
                                    <div id="pinterest" class="pinterest-top" style="display: inline-block;"> <a data-pin-do="buttonBookmark" style="width:30px;" target="_blank" data-pin-custom="true" data-pin-log="button_pinit_bookmarklet" data-pin-href="https://www.pinterest.com/pin/create/button/"><span class="addthis_button_pinterest_share ir  ir-social ir-pinterest" title="Pin This on Pinterest">
                                    </span>            </a>
                                    </div>
                                    <a class="addthis_button_sinaweibo ir  ir-social ir-weibo" href="http://service.weibo.com/share/share.php?url=%23.VQHHV7iznfQ.sinaweibo&amp;title=" onclick="return weibo_share(520, 350)" target="_blank" title="Share This on Weibo"></a>
                                    <a class="addthis_button_linkedin ir  ir-social ir-linkedin" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=&amp;title=" onclick="return linkedin_share(520, 425)" target="_blank" title="Share This on LinkedIn"></a>
                                    <a class="addthis_button_email ir ir-social ir-email" style="cursor: pointer;" title="Share via Email" onclick="javascript:window.location='mailto:?subject='+ encodeURIComponent( $('title').text() ) +'&amp;body=Here\'s a link to an interesting article I discovered on Swarovski-Gemstones\.com: ' + window.location.href;"></a>
                                </div>
                          

                            </div> -->
                        </div>
                    </div>
                    <!--/UdmComment-->
                    <div class="row contentrow">
                        <div class="component-promo featured col-xs-12 col-sm-12">
                            <div class="row">
                                <!-- <div class="col-xs-12">
                                    <div class="image-container">
                                        <img alt="Banner_Dancing_Stone2.jpg" class=" img-responsive lazyloaded" src="/img/news/detail/Banner_Dancing_Stone2.jpg">                            
                                    </div>
                                </div> -->
                                <div class="col-xs-12">
                                    <div class="component-promo-text">                   
                                        <p class="slide-meta hidden">                                                     
                                        </p>             
                                        <h4 class="title"></h4>
                                        <div>
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                          @if($langNetis == 'vi')
                           <div class="component-headline">
                            <div class="headline_title">
                                <h2 style="text-transform: uppercase;">Yêu thích màu Green Light</h2></div>
                            <div class="headline_text">
                                <p><b>Xu hướng hình ảnh đá quý cho mùa xuân / hè 2019</b></p>
                            </div>
                        </div>
                        <div class="component-promo featured col-xs-12 col-sm-12">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="image-container">
                                     <!--       <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/news/detail/Swarovski_Gemstones_-_Dancing_Stone2.mp4" style="width: 100%">
                                             <source type="video/mp4" src="../img/news/detail/Swarovski_Gemstones_-_Dancing_Stone2.mp4">
                                          </video> -->

                                        <img alt="SGB_DesignContest2018_Kostroma_A5_PRINT-1.jpg" class=" img-responsive lazyloaded" src="/img/news/detail/header_greenlights.jpg"> 
                                           
                                       </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="component-promo-text">
                                        <!-- Meta -->
                                        <p class="slide-meta hidden"> </p>
                                        <!-- Promo text -->
                                        <h4 class="title"></h4>
                                        <div>
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="component-promo featured col-xs-12 col-sm-12">
                            <div class="row">
                                <div class="col-xs-12"> </div>
                                <div class="col-xs-12">
                                    <div class="component-promo-text">
                                        <!-- Meta -->
                                        <p class="slide-meta hidden"> </p>
                                        <!-- Promo text -->
                                        <h4 class="title"></h4>
                                        <div>
                                            <p>
                                            Lorde không chỉ hát về việc vượt qua nổi đau trong lòng với bài hát “Green Light” - cô ấy đang dự đoán xu hướng trang sức cho mùa xuân / hè 2019 này đấy. Hàng triệu tín đồ không chỉ chấp nhận Green Light như một ca khúc, màu xanh còn đến với mọi người trong cuộc sống hàng ngày, thể hiện mối quan tâm của con người đối với môi trường và hành tinh dần hình thành xu hướng màu sắc. Thời trang ghi dầu ấn với màu xanh lá, từ những viên ngọc lục bảo đến những sắc thái tươi mới. Thề hiện rõ hơn trong kết hợp của Rihanna với sản phẩm từ Chopard trong không gian xanh của rừng, tượng trưng cho quê hương Barbados của cô.
                                            &nbsp;&nbsp;</p>
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                          @else
                           <div class="component-headline">
                            <div class="headline_title">
                                <h2>YES TO GREEN LIGHT</h2></div>
                            <div class="headline_text">
                                <p><b>A Gem Visions Trend for Spring / Summer 2019</b></p>
                            </div>
                        </div>
                        <div class="component-promo featured col-xs-12 col-sm-12">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="image-container">
                                         <!--   <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/news/detail/Swarovski_Gemstones_-_Dancing_Stone2.mp4" style="width: 100%">
                                             <source type="video/mp4" src="../img/news/detail/Swarovski_Gemstones_-_Dancing_Stone2.mp4">
                                          </video> -->

                                        <img alt="SGB_DesignContest2018_Kostroma_A5_PRINT-1.jpg" class=" img-responsive lazyloaded" src="/img/news/detail/header_greenlights.jpg"> 
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="component-promo-text">
                                        <!-- Meta -->
                                        <p class="slide-meta hidden"> </p>
                                        <!-- Promo text -->
                                        <h4 class="title"></h4>
                                        <div>
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="component-promo featured col-xs-12 col-sm-12">
                            <div class="row">
                                <div class="col-xs-12"> </div>
                                <div class="col-xs-12">
                                    <div class="component-promo-text">
                                        <!-- Meta -->
                                        <p class="slide-meta hidden"> </p>
                                        <!-- Promo text -->
                                        <h4 class="title"></h4>
                                        <div>
                                            <p>Lorde wasn’t just singing about overcoming heartbreak with her bold breakup anthem Green Light — she was also predicting a trend for jewelry for Spring/Summer 2019. Millennials have not only embraced Green Light as an anthem; they’ve embraced going green in their everyday lives, and their concern for the environment and the planet is now shaping color trends. This fashion for green, from deep intense emeralds to fresh, rain-washed shades was further solidified when Rihanna, another millennial icon, announced a collaboration with Chopard using stones in jungle green, symbolic of the lush gardens of her home country of Barbados.&nbsp;&nbsp;</p>

                                            <p>Vibrant green gemstones – ranging from the precious emerald, which has seen a surge in popularity recently, to peridots, garnets and tourmalines – look great combined with rainforest vine-inspired organic silhouettes, or in exotic flower and tropical leaves designs. Looking for an even more elegant look? Pair them with opals and pearls. Contrasting these verdant tones with colorful base metals or stones in other saturated colors also makes them look extra fresh.</p>
                                            <p>The Swarovski Gemstones portfolio, which includes Swarovski Genuine Topaz in the strong Rainforest shade; Swarovski Zirconia in refreshing Fancy Light Green; the emerald-like Swarovski Zirconia Fancy Green; and the pure Swarovski Zirconia Green are all made for leaf or flower settings. All these colors are created using the patented TCF™ process, which enhances the stones with a permanent hard-ceramic layer that protects them during a wide range of setting and casting processes.</p>
                                            <p>And there’s a new star in town, set to draw some green-eyed looks. While Miranda Priestly may scoff that florals for spring aren’t groundbreaking, the zesty new Swarovski Zirconia Spring Green color will bring a millennial modern freshness to any design.</p>
                                           
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                          @endif
                       
             <!--            <div class="component-headline">
                            <hr>
                            <div class="headline_title">
                                <h2>discover more</h2></div>
                            <div class="headline_text"></div>
                        </div>
                        <div class="component-focus-content col-xs-12 col-sm-12 col-md-12" id="component-focus-content-related-content-8938159">
                            <div class="item col-xs-12 col-sm-4 col-md-4   text-center" style="display:block;">
                                <a href="javascript:void(0)" target="">
                                    <div class="image-container"><img class=" img-responsive lazyloaded" src="/img/news/detail/teaser_Gem_Visions.jpg"></div>
                                    <h5 class="title">Gem Visions 2019</h5></a>
                                <p class="focus-link hide">Swarovski works hard to provide customers both with insight into today’s most crucial breaking trends and the incentive they need to forge new creative territory in their designs. </p>
                                <p class="related-link"><a href="javascript:void(0)" target="">get inspired</a></p>
                            </div>
                            <div class="item col-xs-12 col-sm-4 col-md-4  text-center" style="display:block;">
                                <a href="javascript:void(0)" target="_blank">
                                    <div class="image-container"><img class=" img-responsive lazyloaded" src="/img/news/detail/Teaser_SGB_DesignContest2018_Kostroma_A5_PRINT-1.jpg"></div>
                                    <h5 class="title">The competition</h5></a>
                                <p class="focus-link hide"></p>
                                <p class="related-link"><a href="javascript:void(0)" target="_blank">participate here</a></p>
                            </div>
                            <div class="item col-xs-12 col-sm-4 col-md-4  text-center" style="display:block;">
                                <a href="javascript:void(0)" target="">
                                    <div class="image-container"><img class=" img-responsive lazyloaded" src="/img/news/detail/Unbenannt-2_1.jpg"></div>
                                    <h5 class="title">Innovations</h5></a>
                                <p class="focus-link hide">At Swarovski, we endeavor to find inspiring, innovative solutions that add variety, style, and individuality to our extensive product range. Thanks to our detailed research and creative expertise, we continually deliver exciting new materials, colors, shapes, and cuts, as well as inventive new designs and technical features for all of our product categories.</p>
                                <p class="related-link"><a href="javascript:void(0)" target="">see more</a></p>
                            </div>
                        </div> -->
                    </div>

                    <!--UdmComment-->
                </div>
            </div>
            
        </div>

    </div>

</div>
@endsection