@extends('layouts.master')
@section('styles')  
      <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" href="../css/desgin_competition.css">
@endsection
@section('content')
<div class="wrap">

    <div class="content-main" id="content-main">

        <!-- Page Content
        ================================================== -->
        <div class="container container-body">
            <div class="row">

             <!--    <div class="col-xs-12 col-md-2">

                    <div class="component-sidebar-menu component-menu-collapse" id="component-sidebar-menu">
                        <div class="btn-group dropdown">
                            <ul class="dropdown-menu " role="menu" aria-labelledby="component-sidebar-menu">

                                <li class="subnav" role="menuitem">
                                    <h5 class="title ">						<a href="/news/News.en.html" class="">						    News						</a>					    </h5>
                                    <ul class="dropdown-menu">

                                        <li role="menuitem" class="folder"> <a href="/news/Industry_News.en.html" class="">						    Industry News						</a> </li>

                                        <li role="menuitem" class="folder"> <a href="/news/Productstories.en.html" class="">						    Product Stories						</a> </li>

                                        <li role="menuitem" class="folder"> <a href="/news/Innovation_Lab_startpage.en.html" class="">						    Innovation Lab						</a> </li>

                                        <li role="menuitem" class="folder"> <a href="/news/collaborations.en.html" class="">						    Collaborations						</a> </li>

                                        <li role="menuitem" class="folder"> <a href="/news/Inspirations.en.html" class="">						    Inspirations						</a> </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div> -->

                <div class="col-xs-12 col-md-12">
                    <div class="row">
                        <div class="meta-nav backshare col-md-12">
                            <div class="col-xs-3 col-sm-6">
                                <a href="javascript:history.go(-1)" class="upper sub"> <span>&lt; {{trans('products.back')}}</span> </a>
                            </div>
<!--                             <div class="col-xs-9 col-sm-6 text-right">

                          
                                <div class="component-addthis-toolbox addthis_default_style addthis_32x32_style" role="menuitem">
                                    <a class="addthis_button_facebook ir  ir-social ir-facebook" href="http://www.facebook.com/share.php?u=" onclick="return facebook_share(520, 350)" target="_blank" title="Share This on Facebook"></a>
                                    <a class="addthis_button_twitter ir  ir-social ir-twitter" href="http://twitter.com/intent/tweet?status=%20+-+%20" onclick="return twitter_share(520, 350)" target="_blank" title="Share This on Twitter"></a>
                                    <div id="pinterest" class="pinterest-top" style="display: inline-block;"> <a data-pin-do="buttonBookmark" style="width:30px;" target="_blank" data-pin-custom="true" data-pin-log="button_pinit_bookmarklet" data-pin-href="https://www.pinterest.com/pin/create/button/"><span class="addthis_button_pinterest_share ir  ir-social ir-pinterest" title="Pin This on Pinterest">
                                    </span>            </a>
                                    </div>
                                    <a class="addthis_button_sinaweibo ir  ir-social ir-weibo" href="http://service.weibo.com/share/share.php?url=%23.VQHHV7iznfQ.sinaweibo&amp;title=" onclick="return weibo_share(520, 350)" target="_blank" title="Share This on Weibo"></a>
                                    <a class="addthis_button_linkedin ir  ir-social ir-linkedin" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=&amp;title=" onclick="return linkedin_share(520, 425)" target="_blank" title="Share This on LinkedIn"></a>
                                    <a class="addthis_button_email ir ir-social ir-email" style="cursor: pointer;" title="Share via Email" onclick="javascript:window.location='mailto:?subject='+ encodeURIComponent( $('title').text() ) +'&amp;body=Here\'s a link to an interesting article I discovered on Swarovski-Gemstones\.com: ' + window.location.href;"></a>
                                </div>
                          

                            </div> -->
                        </div>
                    </div>
                    <!--/UdmComment-->
                    <div class="row contentrow">
                        <div class="component-headline">
                            <div class="headline_title">
                                <h2>SMALL IS BEAUTIFUL</h2></div>
                            <div class="headline_text">
                                <p><b>Swarovski Presettings in tiny sizes for watch design</b></p>
                            </div>
                        </div>
                        <div class="component-promo featured col-xs-12 col-sm-6">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="image-container">
                                           <video id="vid-videoplayer-cut54758740_html5_api" autoplay="true" loop="true" preload="auto" class="vjs-tech" poster="" data-setup="{}" src="../img/news/detail/Presetting.mp4" style="width: 100%">
                                             <source type="video/mp4" src="../img/news/detail/Presetting.mp4">
                                          </video>

                                       <!--  <img alt="SGB_DesignContest2018_Kostroma_A5_PRINT-1.jpg" class=" img-responsive lazyloaded" src="/img/news/detail/SGB_DesignContest2018_Kostroma_A5_PRINT-1.jpg">  --></div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="component-promo-text">
                                        <!-- Meta -->
                                        <p class="slide-meta hidden"> </p>
                                        <!-- Promo text -->
                                        <h4 class="title"></h4>
                                        <div>
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="component-promo featured col-xs-12 col-sm-6">
                            <div class="row">
                                <div class="col-xs-12"> </div>
                                <div class="col-xs-12">
                                    <div class="component-promo-text">
                                        <!-- Meta -->
                                        <p class="slide-meta hidden"> </p>
                                        <!-- Promo text -->
                                        <h4 class="title"></h4>
                                        <div>
                                            <p><br>Ever creative, the watch industry is often using tiny diamonds and gemstones to accentuate dials and bezels, as of late also in men’s models, according to examples seen at the latest trade fairs. The new Swarovski Super Small Presettings are ideal for designers who are drawn to this trend. These presettings, using extremely small zirconia, genuine gemstones and diamonds in 1 mm, 1.3 mm or 1.5 mm, have been developed especially for the watch industry.&nbsp;&nbsp;</p>
                                            <p>As the stones are already mounted in bezel settings and can be glued onto the dial they are a quick and efficient way to create attractive markings for the minutes/hours in either white or colored stones. Working with gemstones that are already pre-set removes a production step and saves costs. They are also being used to reproduce Star Drop designs that mirror tiny stars in a night sky, or as random stone placement on the bezel. Ideally suited to create luxe but everyday styles, the Swarovski Super Small Presettings come in the base materials Silver or Brass and are ideal for application to solid or thin-walled materials, such as bezels or dials.</p>
                                            <p>
                                                <please></please>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="component-headline">
                            <hr>
                            <div class="headline_title">
                                <h2>discover more</h2></div>
                            <div class="headline_text"></div>
                        </div>
                        <div class="component-focus-content col-xs-12 col-sm-12 col-md-12" id="component-focus-content-related-content-8938159">
                            <div class="item col-xs-12 col-sm-4 col-md-4   text-center" style="display:block;">
                                <a href="javascript:void(0)" target="">
                                    <div class="image-container"><img class=" img-responsive lazyloaded" src="/img/news/detail/teaser_Gem_Visions.jpg"></div>
                                    <h5 class="title">Gem Visions 2019</h5></a>
                                <p class="focus-link hide">Swarovski works hard to provide customers both with insight into today’s most crucial breaking trends and the incentive they need to forge new creative territory in their designs. </p>
                                <p class="related-link"><a href="javascript:void(0)" target="">get inspired</a></p>
                            </div>
                            <div class="item col-xs-12 col-sm-4 col-md-4  text-center" style="display:block;">
                                <a href="javascript:void(0)" target="_blank">
                                    <div class="image-container"><img class=" img-responsive lazyloaded" src="/img/news/detail/Teaser_SGB_DesignContest2018_Kostroma_A5_PRINT-1.jpg"></div>
                                    <h5 class="title">The competition</h5></a>
                                <p class="focus-link hide"></p>
                                <p class="related-link"><a href="javascript:void(0)" target="_blank">participate here</a></p>
                            </div>
                            <div class="item col-xs-12 col-sm-4 col-md-4  text-center" style="display:block;">
                                <a href="javascript:void(0)" target="">
                                    <div class="image-container"><img class=" img-responsive lazyloaded" src="/img/news/detail/Unbenannt-2_1.jpg"></div>
                                    <h5 class="title">Innovations</h5></a>
                                <p class="focus-link hide">At Swarovski, we endeavor to find inspiring, innovative solutions that add variety, style, and individuality to our extensive product range. Thanks to our detailed research and creative expertise, we continually deliver exciting new materials, colors, shapes, and cuts, as well as inventive new designs and technical features for all of our product categories.</p>
                                <p class="related-link"><a href="javascript:void(0)" target="">see more</a></p>
                            </div>
                        </div>
                    </div>

                    <!--UdmComment-->
                </div>
            </div>
        </div>

    </div>

</div>
@endsection