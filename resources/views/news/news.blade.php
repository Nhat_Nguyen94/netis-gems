@extends('layouts.master')
@section('styles')
      <title>{{trans('products.news')}}</title>  
      <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" href="../css/news.css">
@endsection
@section('content')
<?php $langNetis = \App::getLocale(); ?>
<div class="container container-body">
<!--     <div class="row "> --> 
        <p class="text-center" style="margin-bottom: 20px;">
            {{trans('products.please_find_news')}}
            <br>
            Link : <a href="https://www.swarovski-gemstones.com/news/News.en.html">https://www.swarovski-gemstones.com/news/News.en.html</a>
        </p>
<!-- </div> -->
     @if($langNetis == 'vi')
      <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="results">
                <div class="component-focus-content focus-content-news col-xs-12 col-sm-12 col-md-12">
                    <div class="row">   
                        <div class="item col-xs-12 col-sm-4 col-md-4 text-center" style="display:block;">
                            <a href="/desgin_competition">
                                <div class="image-container">
                                    <div class="image-overlay is-loaded" style="display: none;"></div> <img alt="DESIGN COMPETITION" class=" img-responsive lazyloaded" src="/img/news/news1.jpg" > </div>
                                </a>
                                <!-- <p class="slide-meta "><span class="slide-category">Product Stories</span> | <span class="slide-date">4.7.2018</span></p> -->
                                <h5 class="title">
                                    <a href="/desgin_competition">HỘI THI THIẾT KẾ </a>
                                </h5>

                                <div class="focus-link" style="text-transform: capitalize;">
                                Cuộc thi thiết kế để truyền cảm hứng cho thị trường trang sức Nga</div>
                                <p class="related-link">
                                    <a href="/desgin_competition">{{trans('products.read_more')}}</a>
                                </p>
                            </div>
                            <div class="item col-xs-12 col-sm-4 col-md-4 text-center" style="display:block;">
                                <a href="/dancings_stones">
                                    <div class="image-container">
                                        <div class="image-overlay is-loaded" style="display: none;"></div> <img alt="Watch Trends 2019" class=" img-responsive lazyloaded" src="/img/news/Teaser_Dancing_Stone2.jpg"> </div>
                                    </a>
                                    <!-- <p class="slide-meta "><span class="slide-category">Product Stories</span> | <span class="slide-date">29.6.2018</span></p> -->
                                    <h5 class="title">
                                        <a href="/dancings_stones">ĐÁ NHẢY</a>
                                    </h5>
                                    <div class="focus-link">Phong cách mới mang nét trẻ trung tươi vui cho thiết kế</div>
                                    <p class="related-link">
                                        <a href="/dancings_stones">{{trans('products.read_more')}}</a>
                                    </p>
                                </div>
                                <div class="item col-xs-12 col-sm-4 col-md-4 text-center" style="display:block;">
                                    <a href="/yes_to_green_light">
                                        <div class="image-container">
                                            <div class="image-overlay is-loaded" style="display: none;"></div> <img alt="SMALL IS BEAUTIFUL" class=" img-responsive lazyloaded" src="/img/news/Teaser_GemVisions2019_CutsColors_GreenLight_S27.jpg"> </div>
                                        </a>
                                        <!-- <p class="slide-meta "><span class="slide-category">Product Stories</span> | <span class="slide-date">27.6.2018</span></p> -->
                                        <h5 class="title">
                                            <a href="/yes_to_green_light">Yêu thích màu Green Light</a>
                                        </h5>

                                        <div class="focus-link">Xu hướng hình ảnh đá quý cho mùa xuân / hè 2019</div>
                                        <p class="related-link">
                                            <a href="/yes_to_green_light">{{trans('products.read_more')}}</a>
                                        </p>
                                    </div>
                                    @if(isset($news))
                                    @foreach($news as $val)
                                    <div class="item col-xs-12 col-sm-4 col-md-4 text-center" style="display:block;">
                                        <a href="{{route('frontend.news.detail',['slug'=>$val->slug])}}">
                                            <div class="image-container">
                                                <div class="image-overlay is-loaded" style="display: none;"></div> <img alt="SMALL IS BEAUTIFUL" class=" img-responsive lazyloaded"   src="{{isset($val->img) ? $val->img : '/img/news/news3.jpg' }}"> </div>
                                            </a>
                                            <!-- <p class="slide-meta "><span class="slide-category">Product Stories</span> | <span class="slide-date">27.6.2018</span></p> -->
                                            <h5 class="title">
                                                <a href="{{route('frontend.news.detail',['slug'=>$val->slug])}}">{{$val->title_vi}}</a>
                                            </h5>

                                            <div class="focus-link">{{$val->recommend_vi}}</div>
                                            <p class="related-link">
                                                <a href="{{route('frontend.news.detail',['slug'=>$val->slug])}}">{{trans('products.read_more')}}</a>
                                            </p>
                                        </div>
                                        @endforeach
                                        @endif
                                </div>
                            </div>      
                        </div>
                       <!--  <div class="component-results-footer row">
                            <div class="col-md-12 show-more-results">
                                <a href="javascript:void(0)" class="btn btn-default show-more-results" type="button">Xem Thêm</a>
                            </div>
                        </div> -->      
                    </div>
                </div>
     @else
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="results">
                <div class="component-focus-content focus-content-news col-xs-12 col-sm-12 col-md-12">
                    <div class="row">	
                        <div class="item col-xs-12 col-sm-4 col-md-4 text-center" style="display:block;">
                            <a href="/desgin_competition">
                                <div class="image-container">
                                    <div class="image-overlay is-loaded" style="display: none;"></div> <img alt="DESIGN COMPETITION" class=" img-responsive lazyloaded" src="/img/news/news1.jpg" > </div>
                                </a>
                                <!-- <p class="slide-meta "><span class="slide-category">Product Stories</span> | <span class="slide-date">4.7.2018</span></p> -->
                                <h5 class="title">
                                    <a href="/desgin_competition">DESIGN COMPETITION</a>
                                </h5>

                                <div class="focus-link">Design Competition to Inspire the Russian Jewelry Market</div>
                                <p class="related-link">
                                    <a href="/desgin_competition">{{trans('products.read_more')}}</a>
                                </p>
                            </div>
                            <div class="item col-xs-12 col-sm-4 col-md-4 text-center" style="display:block;">
                                <a href="/dancings_stones">
                                    <div class="image-container">
                                        <div class="image-overlay is-loaded" style="display: none;"></div> <img alt="Watch Trends 2019" class=" img-responsive lazyloaded" src="/img/news/Teaser_Dancing_Stone2.jpg"> </div>
                                    </a>
                                    <!-- <p class="slide-meta "><span class="slide-category">Product Stories</span> | <span class="slide-date">29.6.2018</span></p> -->
                                    <h5 class="title">
                                        <a href="/dancings_stones">DANCING STONES</a>
                                    </h5>
                                    <div class="focus-link">Innovative preset element adds playfulness to designs</div>
                                    <p class="related-link">
                                        <a href="/dancings_stones">{{trans('products.read_more')}}</a>
                                    </p>
                                </div>
                                <div class="item col-xs-12 col-sm-4 col-md-4 text-center" style="display:block;">
                                    <a href="/yes_to_green_light">
                                        <div class="image-container">
                                            <div class="image-overlay is-loaded" style="display: none;"></div> 
                                            <img alt="SMALL IS BEAUTIFUL" class=" img-responsive lazyloaded" src="/img/news/Teaser_GemVisions2019_CutsColors_GreenLight_S27.jpg" > </div>
                                        </a>
                                        <!-- <p class="slide-meta "><span class="slide-category">Product Stories</span> | <span class="slide-date">27.6.2018</span></p> -->
                                        <h5 class="title">
                                            <a href="/yes_to_green_light">YES TO GREEN LIGHT</a>
                                        </h5>

                                        <div class="focus-link">A Gem Visions Trend for Spring / Summer 2019</div>
                                        <p class="related-link">
                                            <a href="/yes_to_green_light">{{trans('products.read_more')}}</a>
                                        </p>
                                    </div>
                                </div>
                            </div>		
                        </div>
                       <!--  <div class="component-results-footer row">
                            <div class="col-md-12 show-more-results">
                                <a href="javascript:void(0)" class="btn btn-default show-more-results" type="button">Show more News</a>
                            </div>
                        </div> -->      
                    </div>
                </div>
        @endif
    </div>
@endsection