@extends('admin.layouts.master')

@section('content')
 <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Tables
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> Tables
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <h2>Bordered Table</h2>
                        <a class="btn btn-primary" href="{{route('admin.category.created')}}" role="button">Thêm Mới</a>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name Vi</th>
                                        <th>Name En</th>
                                        <th>Status</th>
                                        <th>Tools</th>
                                        <th>Tools</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach($category as $value)
                                	<tr>
                                		<td>{{$value->id}}</td>
                                		<td>{{$value->name_vi}}</td>
                                		<td>{{$value->name_en}}</td>
                                		<td>@if($value->status == '2' )
                                        Show
                                        @else
                                        Hidden
                                        @endif
                                        </td>
                                		 <td><a class="btn btn-danger" href="{{route('admin.category.destroy',['id'=>$value->id])}}" role="button">Delete</a></td>
                                        <td><a class="btn btn-info" href="{{route('admin.category.edit',['id'=>$value->id])}}" role="button">Updated</a></td>
                                	</tr>
                           			@endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.row -->


            </div>
@endsection