<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<title>SB Admin - Bootstrap Admin Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/admin/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
<!--     <link href="/admin/css/plugins/morris.css" rel="stylesheet"> -->

    <!-- Custom Fonts -->
    <link href="/admin/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    @yield('styles')
</head>
<body>
	<div id="wrapper">

        <!-- Navigation -->
       @include('admin.layouts.include.menu')

        <div id="page-wrapper">
		@yield('content')
         
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="/admin/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/admin/js/bootstrap.min.js"></script>
    <script>
       $(document).ready(function() {
            $('.collapse .navbar-nav li a').each(function(index) {
                if(this.href.trim() == window.location)
                $(this).parent('li').addClass("active");
            });
        });
            
    </script>

    <!-- Morris Charts JavaScript -->
<!--     <script src="/admin/js/plugins/morris/raphael.min.js"></script>
    <script src="/admin/js/plugins/morris/morris.min.js"></script>
    <script src="/admin/js/plugins/morris/morris-data.js"></script> -->
    @yield('scripts')
</body>
</html>