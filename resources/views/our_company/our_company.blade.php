@extends('layouts.master')
@section('styles')
      <title>{{trans('products.our_company')}}</title>
      <link rel="stylesheet" href="css/our_company.css">
      <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
@endsection
@section('content')
<?php $langNetis = \App::getLocale(); ?>
    <div class="container container-body">
         <!--/UdmComment-->
         <div class="row">
            <div class="carousel-article col-md-12 hero">
               <div class="component-carousel carousel-title hero" id="component-carousel-hero-5292817" >
                  <div id="carousel-hero-5292817" class="carousel slide carousel-title">
                     <!-- Indicators -->  
                     <ol class="carousel-indicators hide">
                        <li  data-slide-to="0" class="active"></li>
                     </ol>
                     <div class="carousel-inner">
                        <div class="item active" id="carousel-carousel-hero-5292817-item-0">
                           <!--START carousel item-->
                           <div class="image-container" data-href="">
                              <div class="image-overlay is-loaded" ></div>
                              <a href="">
                                 <div class="overlay" style="position:absolute; right:180px; width:350px; height:auto; z-index:100;">
                                    <h1>
                                       <span style="position:relative; font-size:16px; color:#fff; text-decoration:none;"></span>
                                    </h1>
                                    <br>
                                    <span style="position:relative; font-size:16px; top:-50px; color:#ccc; text-decoration:none;"></span>
                                 </div>
                              </a>
                              <img alt="Genuine Gemstones and Created Stones" class=" img-responsive lazyloaded" src="img/our_company/company_bg.jpg">
                           </div>
                           <div class="container">
                              <div class="carousel-caption hide">
                                 <!-- Meta -->       
                                 <p class="slide-meta ">                  </p>
                                 <h1 class="h2 slide-title"> {{trans('products.our_company')}}</h1>
                              </div>
                           </div>
                           <!--END carousel item-->
                        </div>
                     </div>
                     <!-- Controls -->  <a class="left carousel-control hide" href="#carousel-hero-5292817">
                     <span class="ir ir-chevron ir-chevron-left ir-theme-light"></span></a> 
                     <a class="right carousel-control hide" href="#carousel-hero-5292817" data-slide="next"><span class="ir ir-chevron ir-chevron-right ir-theme-light"></span></a>  
                  </div>
                  <div class="carousel-heading ">
                     <!-- Meta -->        
                     <p class="slide-meta ">          </p>
                     <h1 class="h2 slide-title text-center" style="font-size: 38px !important;"> {{trans('products.our_company')}}</h1>
                  </div>
               </div>
            </div>
         </div>
               @if($langNetis == 'vi')
               <p style="text-align: center;">Netisgems là nhà phân phối hàng đầu trong việc cung cấp đá quý hiện đại và chất lượng cho các nhà sản xuất trang sức tại Việt Nam. Với niềm đam mê và chuyên môn của mình, chúng tôi sử dụng đá quý Swarovski để cung cấp cho khách hàng  mang lại sự độc đáo,nhiều lựa chọn và chất lượng vượt trội.
Netis Gems tự hào về danh tiếng của mình với các tiêu chuẩn chính xác và hợp tác độc quyền để cung cấp cho khách hàng tất cả các nguyên liệu cần thiết  từ Swarovski để sản xuất đồ trang sức tuyệt hảo..
Chúng tôi cam kết thực hiện các tiêu chuẩn cao nhất nhằm có được sự hài lòng của khách hàng, liên tục cải thiện các dịch vụ của chúng tôi. Đội ngũ chuyên nghiệp của chúng tôi sẽ làm hết sức mình để đáp ứng các nhu cầu của quý khách.
 
               </p>

               @else

               <p style="text-align: center;">Netisgems leads the way in supplying quality contemporary gemstones for designer jewellers in Vietnam. With our passion and expertise in the industry, we use Swarovski Gemstones to provide you with originality, choice and exceptional quality all under one roof. 
               Netis Gems is proud of its reputation for exacting standards and exclusive collaborations to provide you with all the raw material you need to produce exceptional fine jewellery.
               We are strongly committed to the highest standards of customer satisfaction by continually improving our services. Our dedicated team will do their utmost to help you with all of your questions and needs. 
               </p>
               @endif
         <!-- 
            end row bg
            -->
<div class="row">
   <div class="content col-xs-12">
      <div class=" contentrow noFormatting noLists noLinks">
   <!--       <div class="component-promo featured col-xs-12 col-sm-6">
            <div class="row black">
               <div class="col-xs-12">
                  <a href="/company/Swarovski_Gemstones_Business.en.html" target="">
                     <div class="image-container">
                        <img alt="Craftmanship" class=" img-responsive lazyloaded" src="img/our_company/Unbenannt-3.jpg" >                            
                     </div>
                  </a>
               </div>
               <div class="col-xs-12 text-center">
                  <div class="component-promo-text">       
                     <p class="slide-meta "></p>                   
                     <h4 class="title">SWAROVSKI GEMSTONES BUSINESS<br></h4>
                     <div></div>
                     <p class="related-link">                            <a href="/company/Swarovski_Gemstones_Business.en.html" target="">                                discover more                            </a>                        </p>
                  </div>
               </div>
            </div>
         </div> -->
         <style >
            .featured{
               float:left;
            }
         </style>
     <!--     <div class="component-promo featured col-xs-12 col-sm-6">
            <div class="row black">
               <div class="col-xs-12 col-sm-7 col-md-7 col-sm-float-right col-md-float-right" >
                  <a href="/company/Swarovski_Group.en.html" target="">
                     <div class="image-container">
                        <img alt="Swarovski" class=" img-responsive lazyloaded" src="img/our_company/swa.jpg" >                            
                     </div>
                  </a>
               </div>
               <div class="col-xs-12 col-sm-5 vertical-center text-center" >
                  <div class="component-promo-text">
                                  
                     <p class="slide-meta ">                                                                                </p>
                                         
                     <h4 class="title">SWAROVSKI GROUP<br></h4>
                     <div>
                        <p>
                           <please></please>
                        </p>
                     </div>
                     <p class="related-link">                            <a href="/company/Swarovski_Group.en.html" target="">                                find out more                            </a>                        </p>
                  </div>
               </div>
            </div>
             <div class="row black" style="margin-top: 50px;">
               <div class="col-xs-12 col-sm-7 col-md-7">
                  <a href="/company/Corporate_Social_Responsibility.en.html" target="">
                     <div class="image-container">
                        <img alt="CSR" class=" img-responsive lazyloaded" src="img/our_company/social_responsibility.jpg" >                            
                     </div>
                  </a>
               </div>
               <div class="col-xs-12 col-sm-5 col-md-5 vertical-center text-center">
                  <div class="component-promo-text">                      
                     <p class="slide-meta "></p>                      
                     <h4 class="title">CORPORATE SOCIAL RESPONSIBILITY<br></h4>
                     <div>
                        <p>
                           <please></please>
                        </p>
                     </div>
                     <p class="related-link">                            <a href="/company/Corporate_Social_Responsibility.en.html" target="">                                find out more                            </a>                        </p>
                  </div>
               </div>
            </div>
         </div> -->
      <!--    <div class="component-promo featured col-xs-12 col-sm-6">
            <div class="row black">
               <div class="col-xs-12">
                  <a href="/company/Swarovski_Foundation.en.html" target="">
                     <div class="image-container">
                        <img alt="Room_to_Read_India_4.jpg" class=" img-responsive lazyloaded" src="img/our_company/FOUNDATION.jpg" >                            
                     </div>
                  </a>
               </div>
               <div class="col-xs-12 text-center">
                  <div class="component-promo-text">     
                     <p class="slide-meta ">
                     <span class="slide-category">Our Company</span> 
                     <span class="slide-date">3.3.2017</span>
                     </p>             
                     <h4 class="title">SWAROVSKI FOUNDATION</h4>
                     <div>
                     <p>
                           <please></please>
                     </p>
                     </div>
                     <p class="related-link">                            <a href="/company/Swarovski_Foundation.en.html" target="">                                discover more                            </a>                        </p>
                  </div>
               </div>
            </div>
         </div> -->
  <!--        <div class="component-promo featured col-xs-12 col-sm-6 col-sm-float-right col-md-float-right">
            <div class="row black">
               <div class="col-xs-12">
                  <a href="/company/History.en.html" target="">
                     <div class="image-container">
                        <img alt="Daniel Swarovski" class=" img-responsive lazyloaded" src="img/our_company/history.jpg" >                            
                     </div>
                  </a>
               </div>
               <div class="col-xs-12 text-center">
                  <div class="component-promo-text">                    
                     <p class="slide-meta ">                                                                                </p>                     
                     <h4 class="title">HISTORY</h4>
                     <div>
                        <p>
                           <please></please>
                        </p>
                     </div>
                     <p class="related-link">                            <a href="/company/History.en.html" target="">                                read more                            </a>                        </p>
                  </div>
               </div>
            </div>
         </div> -->
         <div class="component-promo featured col-xs-12 col-sm-6">
           
         </div>
         <div class="component-headline">
            <hr>
            <div class="headline_title">
               <h2>{{trans('products.discover_more')}}</h2>
            </div>
            <div class="headline_text"></div>
         </div>
         <div class="component-focus-content col-xs-12 col-sm-12 col-md-12" id="component-focus-content-related-content-5442010">
            <div class="row">
            <div class="item col-xs-12 col-sm-4 col-md-4   text-center" style="display:inline-block;">
               <a href="/ourProducts" target="">
                  <div class="image-container"><img class=" img-responsive lazyloaded" src="img/our_company/Teaser_sRGB_SGE_Zirconia_Innovations_2018_Final.jpg" ></div>
                  <h5 class="title"> {{trans('products.products_home')}}</h5>
               </a>
               <p class="focus-link hide">Swarovski offers exquisite Genuine Gemstones and Created Stones made from the purest raw materials and crafted to the highest standards for quality, color, size and shape.</p>
               <p class="related-link"><a href="/ourProducts" target="">{{trans('products.find_out_more')}}</a></p>
            </div>
            <div class="item col-xs-12 col-sm-4 col-md-4  text-center" style="display:inline-block;">
               <a href="/our-company" target="">
                  <div class="image-container"><img class=" img-responsive lazyloaded" src="img/our_company/teaser_brands.jpg"></div>
                  <h5 class="title">{{trans('products.our_company')}}</h5>
               </a>
               <p class="focus-link hide">The Swarovski Group operates a global branding program which enables Swarovski branding partners to use one of the company’s ingredient brands Zirconia from Swarovski® and Gemstones from Swarovski® as an independent quality endorsement on the partner’s products.</p>
               <p class="related-link"><a href="/our-company" target="">{{trans('products.find_out_more')}}</a></p>
            </div>
            <div class="item col-xs-12 col-sm-4 col-md-4  text-center" style="display:inline-block;">
               <a href="/contact-us" target="">
                  <div class="image-container"><img class=" img-responsive lazyloaded" src="img/our_company/Findus.jpg" ></div>
                  <h5 class="title">{{trans('products.contact_us')}}</h5>
               </a>
               <p class="focus-link hide">Sales Offices, where to buy</p>
               <p class="related-link"><a href="/contact-us" target="">{{trans('products.find_out_more')}}</a></p>
            </div>
            </div>
         </div>
         <p><br></p>
      </div>
   </div>
</div>
         <!-- end content -->
      </div>
@endsection