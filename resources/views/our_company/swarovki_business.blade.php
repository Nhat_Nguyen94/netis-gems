@extends('layouts.master')
@section('styles')
      <link rel="stylesheet" href="css/company_business.css">
      <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
@endsection
@section('content')
<div class="wrap">
<div class="content-main" id="content-main">
   <!-- Page Content -->
   <div class="container container-body">
      <div class="row">
         <div class="col-xs-12 col-md-2">
            <div class="component-sidebar-menu component-menu-collapse" id="component-sidebar-menu">
               <div class="btn-group dropdown">
                  <ul class="dropdown-menu " role="menu" aria-labelledby="component-sidebar-menu">
                     <li class="subnav" role="menuitem">
                        <h5 class="title ">                 <a href="/company/Our_Company.en.html" class="">                      Our Company                  </a>                  </h5>
                        <ul class="dropdown-menu">
                           <li role="menuitem" class="folder">                <a href="/company/Swarovski_Gemstones_Business.en.html" class="active">                    Swarovski Gemstones Business                </a>                        </li>
                           <li role="menuitem" class="folder">                <a href="/company/Swarovski_Group.en.html" class="">                     Swarovski Group                 </a>                        </li>
                           <li role="menuitem" class="folder">                <a href="/company/History.en.html" class="">                    History                </a>                        </li>
                           <li role="menuitem" class="folder">                <a href="/company/Corporate_Social_Responsibility.en.html" class="">                    CSR                 </a>                        </li>
                           <li role="menuitem" class="folder">                <a href="/company/Swarovski_Foundation.en.html" class="">                      Swarovski Foundation                  </a>                        </li>
                        </ul>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
         <!--/UdmComment-->
         <div class="col-xs-12 col-md-10 no-padding-left no-padding-right">
            <div class="component-promo featured hero col-xs-12 col-md-12 no-margin">
               <div class="row">
                  <div class="col-xs-12">
                     <div class="image-container">
                        <div class="image-overlay is-loaded" style="display: none;"></div>
                        <img alt="Swarovski Gemstones" class=" img-responsive lazyloaded" src="../img/our_company/business/Banner_Zirconia_ohneTag_72dpi_final3.jpg" >                     
                     </div>
                  </div>
                  <div class="col-xs-12 col-sm-offset-1 col-sm-10 text-center" style="margin: 0 auto">
                     <div class="component-promo-text">
                        <p class="slide-meta hidden"></p>
                        <p>Whenever we look at our products, we see a
                           story unfold before us: the long and successful history of Swarovski, a company
                           whose reputation for innovation, cutting precision, unrivaled quality and
                           service has been at the forefront of the international market since 1895.
                           Today, Swarovski applies 120 years of technical expertise and creative vitality
                           to a vast and colorful world of inspiring products – from jewelry, watches, and
                           eyewear, to selected electronic devices. Founded in 1965, the Swarovski Gemstones
                           Business leads the field worldwide, employing only the finest raw materials and
                           the strictest manufacturing standards to conceive our Genuine Gemstones and
                           Created Stones.&nbsp;<br>
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <hr>
            <div class="component-promo featured col-xs-12 text-center">
               <h2 class="title alt"></h2>
               <div class="col-md-8 col-md-offset-2 text-center"></div>
            </div>
            <div class="component-promo featured col-xs-12 col-sm-12">
               <div class="row">
                  <div class="col-xs-12 col-sm-7 col-md-7">
                     <div class="image-container">
                        <img alt="Pioneering Spirit" class=" img-responsive lazyloaded" src="../img/our_company/business/Zirconia_Innovations_2018_Final_web.jpg">                            
                     </div>
                  </div>
                  <div class="col-xs-12 col-sm-5 col-md-5 vertical-center text-center">
                     <div class="component-promo-text">
                        <!-- Meta -->                        
                        <p class="slide-meta hidden">                                                                                </p>
                        <!-- Promo text -->                        
                        <h4 class="title"></h4>
                        <div>
                           <p>We cut, and polish exclusively on our own
                              company premises, guaranteeing outstanding results in shape, color, and
                              brilliance, and meeting the topmost social and environmental standards.&nbsp;
                           </p>
                           <p>
                              <please></please>
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="component-promo featured col-xs-12 col-sm-12 col-sm-float-right col-md-float-right">
               <div class="row black">
                  <div class="col-xs-12 col-sm-7 col-md-7 col-sm-float-right col-md-float-right" >
                     <div class="image-container">
                        <img alt="Craftmanship" class=" img-responsive lazyloaded" src="../img/our_company/business/Unbenannt-2.png" >                            
                     </div>
                  </div>
                  <div class="col-xs-12 col-sm-5 vertical-center text-center" >
                     <div class="component-promo-text">
                        <!-- Meta -->                        
                        <p class="slide-meta hidden">                                                                                </p>
                        <!-- Promo text -->                        
                        <h4 class="title"></h4>
                        <div>
                           <p>Our dedication and
                              craftsmanship are reflected in each and every one of our unique stones,
                              providing creative diversity and individual product solutions for a host of
                              international brands, designers, and manufacturers.&nbsp;
                           </p>
                           <p>
                              <please></please>
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
            <div class="component-promo featured col-xs-12 col-sm-6">
               <div class="row">
                  <div class="col-xs-12 col-sm-7 col-md-7" style="height: 291px;">
                     <a href="/company/History.en.html" target="">
                        <div class="image-container">
                           <img alt="Swarovski Zirconia" class=" img-responsive lazyloaded" src="../img/our_company/business/_Press_Picture_Swarovski_Zirconia_120_Facets_Cut_4.jpg" >                            
                        </div>
                     </a>
                  </div>
                  <div class="col-xs-12 col-sm-5 col-md-5 vertical-center text-center" style="height: 291px;">
                     <div class="component-promo-text">
                        <!-- Meta -->                        
                        <p class="slide-meta ">                                                                                </p>
                        <!-- Promo text -->                        
                        <h4 class="title">50+ YEARS and counting<br></h4>
                        <div>
                           <p>
                              <please></please>
                           </p>
                        </div>
                        <p class="related-link">                            <a href="/company/History.en.html" target="">                                find out more                            </a>                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="component-promo featured col-xs-12 col-sm-6">
               <div class="row">
                  <div class="col-xs-12 col-sm-7 col-md-7" style="height: 289px;">
                     <a href="/company/Corporate_Social_Responsibility.en.html" target="">
                        <div class="image-container">
                           <img alt="Corporate Social Responsibility" class=" img-responsive lazyloaded" src="../img/our_company/business/4_3_8_Sq1x_SustainabilityReport_2.jpg">                            
                        </div>
                     </a>
                  </div>
                  <div class="col-xs-12 col-sm-5 col-md-5 vertical-center text-center" style="height: 289px;">
                     <div class="component-promo-text">
                        <!-- Meta -->                        
                        <p class="slide-meta ">                                                                                </p>
                        <!-- Promo text -->                        
                        <h4 class="title">Corporate Social Responsibility<br></h4>
                        <div>
                           <p>
                              <please></please>
                           </p>
                        </div>
                        <p class="related-link">                            <a href="/company/Corporate_Social_Responsibility.en.html" target="">                                read more                            </a>                        </p>
                     </div>
                  </div>
               </div>
            </div>
            </div>
            <div class="component-headline">
               <hr>
               <div class="headline_title">
                  <h2>Discover more</h2>
               </div>
               <div class="headline_text"></div>
            </div>
            <div class="component-focus-content col-xs-12 col-sm-12 col-md-12" id="component-focus-content-related-content-5442006">
          <div class="row">
            <div class="item col-xs-12 col-sm-4 col-md-4   text-center" style="display:inline-block;">
               <a href="/products/Our_Products.en.html" target="">
                  <div class="image-container"><img class=" img-responsive lazyloaded" src="../img/our_company/Teaser_sRGB_SGE_Zirconia_Innovations_2018_Final.jpg" ></div>
                  <h5 class="title">Our Products</h5>
               </a>
               <p class="focus-link hide">Swarovski offers exquisite Genuine Gemstones and Created Stones made from the purest raw materials and crafted to the highest standards for quality, color, size and shape.</p>
               <p class="related-link"><a href="/products/Our_Products.en.html" target="">find out more</a></p>
            </div>
            <div class="item col-xs-12 col-sm-4 col-md-4  text-center" style="display:inline-block;">
               <a href="/brand/Our_Brands.en.html" target="">
                  <div class="image-container"><img class=" img-responsive lazyloaded" src="../img/our_company/teaser_brands.jpg"></div>
                  <h5 class="title">Our Brands</h5>
               </a>
               <p class="focus-link hide">The Swarovski Group operates a global branding program which enables Swarovski branding partners to use one of the company’s ingredient brands Zirconia from Swarovski® and Gemstones from Swarovski® as an independent quality endorsement on the partner’s products.</p>
               <p class="related-link"><a href="/brand/Our_Brands.en.html" target="">find out more</a></p>
            </div>
            <div class="item col-xs-12 col-sm-4 col-md-4  text-center" style="display:inline-block;">
               <a href="/contact-us/wheretobuy.en.html" target="">
                  <div class="image-container"><img class=" img-responsive lazyloaded" src="../img/our_company/Findus.jpg" ></div>
                  <h5 class="title">OUR OFFICES</h5>
               </a>
               <p class="focus-link hide">Sales Offices, where to buy</p>
               <p class="related-link"><a href="/contact-us/wheretobuy.en.html" target="">FIND US</a></p>
            </div>
            </div>
            </div>
            <!--UdmComment-->

            <!--/UdmComment-->
         </div>
         <!--UdmComment-->
      </div>
      <!-- Return To Top ================================================== -->
      <div class="component-return-to-top clearfix" id="component-return-to-top">
         <button type="button" class="btn btn-link btn-return-to-top pull-right hidden" style="right: -32.5px; bottom: 0px; display: inline-block;">
         <span>↑</span>
         </button>
      </div>
   </div>
</div>
</div>
@endsection