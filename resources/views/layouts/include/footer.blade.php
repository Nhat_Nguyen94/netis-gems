<div id="footer" class="clear containerFix loggedOutFooter">
    <div id="mainFooter">
        <div id="mainFooterContent">

            <div id="loggedOutfooterLinks">
                <ul class="center inlineBlock">
                    <li><a href="/our-company">{{trans('products.our_company')}}</a></li>
                    <!-- <li><a href="/history" >{{trans('products.about_swarovski')}} </a></li> -->
                    <li><a href="/csr" >CSR </a></li>
                 <!--    <li><a href="https://www.stuller.com/compliance/" >Compliance</a></li>
                    <li><a href="https://www.stuller.com/privacy/" >Privacy Policy</a></li>
                    <li><a href="https://www.stuller.com/careers/" >Stuller Careers</a></li>
                    <li><a href="https://www.stuller.com/pages/2906/" >Gemvision and TDM Solutions</a></li>
                    <li><a href="https://www.stuller.com/international/" >International Enquiries</a></li> -->
                    <li><a href="/contact-us">{{trans('products.contact_us')}}</a></li>
                    <!-- <li><a href="https://www.stuller.com/websitehelp/">Website Help</a></li> -->
                </ul>                    
            </div>               
            <div class="containerFix bottomMarginLarge text-center">

                <!--  ALTERNATIVE SOCIAL MEDIA ICONS -->
           <!--      <div class="altSocialIcons">
                    <a title="Facebook" target="_blank" href="https://www.facebook.com/StullerInc">
                        <span class="fa-stack">
                            <i class="fa fa-circle fa-stack-2x" style="color:#3b5998"></i>
                            <i class="fa fa-facebook fa-stack-1x fa-inverse" style="top:1px;font-size:1.2em;"></i>  
                        </span>
                    </a>

                    <a title="Twitter" target="_blank" href="https://www.twitter.com/StullerInc" >
                        <span class="fa-stack">
                            <i class="fa fa-circle fa-stack-2x" style="color:#55acee"></i>
                            <i class="fa fa-twitter fa-stack-1x fa-inverse" style="font-size:1.2em;"></i>  
                        </span>
                    </a>

                    <a title="Pinterest" target="_blank" href="https://www.pinterest.com/StullerInc" >
                        <span class="fa-stack">
                            <i class="fa fa-circle fa-stack-2x" style="color:#bd081c"></i>
                            <i class="fa fa-pinterest-p fa-stack-1x fa-inverse" style="font-size:1.5em;top:3px;"></i>  
                        </span>
                    </a>

                    <a title="Instagram" target="_blank" href="https://www.instagram.com/StullerInc">
                        <span class="fa-stack">
                            <i class="fa fa-circle fa-stack-2x" style="color:#3f729b"></i>
                            <i class="fa fa-instagram fa-stack-1x fa-inverse" style="font-size:1.2em;"></i>  
                        </span>
                    </a>

                    <a title="YouTube" class="hide" target="_blank" href="https://www.youtube.com/StullerInc">
                        <span class="fa-stack">
                            <i class="fa fa-circle fa-stack-2x" style="color:white;text-shadow:-1px -1px 0 #ccc, 1px -1px 0 #ccc, -1px 1px 0 #ccc, 1px 1px 0 #CCC;"></i>
                            <i class="fa fa-youtube fa-stack-1x" style="color:#cd201f; font-size:17px;"></i>  
                        </span>
                    </a>

                    <a title="YouTube" target="_blank" href="https://www.youtube.com/StullerInc" onclick="TrackFooter('False', 'False', 'Follow Us - YouTube')">
                        <span class="fa-stack">
                            <i class="fa fa-circle fa-stack-2x" style="color:#cd201f;"></i>
                            <i class="fa fa-youtube-play fa-stack-1x fa-inverse" style="font-size:1.1em;top:1px;"></i>  
                        </span>
                    </a>
                </div> -->
            </div>                  
        </div>
    </div>
</div>

<div id="myBtn" class="scrollToTop">
    <span class="fa fa-chevron-up"></span><span>TOP</span></div>