@extends('layouts.master')
@section('styles')
  <title>{{trans('products.genuine_gemstones')}}</title>
<link rel="stylesheet" href="css/genuine_gemstones.css">
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
@endsection
@section('content')
<?php $langNetis = \App::getLocale(); ?>
<div class="wrap">
   <div class="content-main" id="content-main">
      <div class="container-fluid container-body">
         <div class="row">
            <div class="col-xs-12 col-md-2">
               @include('our_products.menu_left')
            </div>
            <div class="col-xs-12 col-md-10 no-padding-left no-padding-right">
               <div class="component-promo featured hero col-xs-12 col-md-12 no-margin">
                  <div class="row">
                     <div class="col-xs-12">
                        <div class="image-container">
                           <div class="image-overlay is-loaded" style="display: none;"></div>
                           <img alt="Genuine Gemstones" class="img-responsive lazyloaded" src="img/gemstones/bg_img.jpg">           
                        </div>
                     </div>
                     <div class="col-xs-12 col-sm-offset-1 col-sm-10 text-center" style="margin: 0 auto;">
                        <div class="component-promo-text">
                           <p class="slide-meta hidden"></p>
                           @if($langNetis == 'vi')
                             <p>Vẻ đẹp của đá quý tự nhiên phụ thuộc vào những đường cắt hoàn hảo. Nổi tiếng là công ty đầu thế giới về việc cắt đá trang sức, Swarovski mang lại vẻ đẹp thực sự của đá quý . Đá được cắt và đánh bóng trong nhà máy Swarovski có công nghệ tinh vi , nơi có những nghiên cứu và phát triển tiên tiến nhầm duy trì các tiêu chuẩn sản phẩm nghiêm ngặt nhất.
                              <br>
                              <br>
                              Đối với Swarovski  chất lượng thật không chỉ cung cấp những sản phẩm hoàn mỹ , công ty luôn cố gắng bảo vệ thiên nhiên bằng cam kết đạt tiêu chuẩn sản xuất sinh thái và có trách nhiệm với xã hội bằng qua qui trình sản xuất đạt chuẩn
                              <br>
                              <a target="_blank" href="https://www.swarovski-gemstones.com/download/SWAROVSKI_GEMSTONES_2018_ONLINE-CATALOG.pdf" class="" >Tham khảo Catalog 2018/2019 &nbsp;</a><br></p>
                           @else
                           <p>The beauty of a high quality gemstone depends on a perfect cut. Renowned as the world leader in the precision-cutting of jewelry stones, Swarovski reveals the true beauty of genuine gemstones, creating brilliant, superlative results. Each of Swarovski’s Genuine Gemstones reflects the quality, creativity, and technical precision that the name Swarovski has come to stand for. Cut and polished in Swarovski’s own factories, the most sophisticated technology and advanced research and development are used to maintain strictest product standards. 
                           <br>
                           <br>
                           But for Swarovski true quality means more than just providing the perfect cut, clarity and color - always trying to protect the gifts of nature, Swarovski is committed to ecologically and socially responsible production standards. As a certified member of the Responsible Jewellery Council, Swarovski can guarantee a responsible production of their genuine gemstones.
                           .<br><a target="_blank" href="https://www.swarovski-gemstones.com/download/SWAROVSKI_GEMSTONES_2018_ONLINE-CATALOG.pdf" class="" >View Genuine Gemstones Product Catalog 2018/2019&nbsp;</a><br></p>
                           @endif
                        </div>
                     </div>
                  </div>
               </div>
               <hr>
               <div class="component-promo featured col-xs-12 text-center">
                  <h2 class="title alt" style="text-transform: uppercase;">SWAROVSKI {{trans('products.genuine_gemstones')}}<br></h2>
                  <div class="col-md-8 col-md-offset-2 text-center"></div>
               </div>
               <div class="row">
                  <div class="component-promo featured col-xs-12 col-sm-4">
                     <div class="row black">
                        <div class="col-xs-12">
                           <a href="/sapphire" target="">
                              <div class="image-container">
                                 <img alt="Sapphire" class="img-responsive lazyloaded" src="img/gemstones/bluesapphire.png">                            
                              </div>
                           </a>
                        </div>
                        <div class="col-xs-12 text-center">
                           <div class="component-promo-text">
                              <p class="slide-meta ">                                                                                </p>
                              <h4 class="title">swarovski GENUINE<br>SAPPHIRE<br></h4>
                              <div>
                                 <p><br></p>
                              </div>
                              <p class="related-link">                            <a href="/sapphire" target="">                                {{trans('products.read_more')}}                            </a>                        </p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="component-promo featured col-xs-12 col-sm-4">
                     <div class="row black">
                        <div class="col-xs-12">
                           <a href="/ruby" target="">
                              <div class="image-container">
                                 <img alt="Ruby" class=" img-responsive lazyloaded" src="img/gemstones/Ruby.jpg">                            
                              </div>
                           </a>
                        </div>
                        <div class="col-xs-12 text-center">
                           <div class="component-promo-text">
                              <p class="slide-meta ">                                                                                </p>
                              <h4 class="title">swarovski GENUINE&nbsp;<br>RUBY<br></h4>
                              <div>
                                 <p><br></p>
                              </div>
                              <p class="related-link">                            <a href="/ruby" target="">                                {{trans('products.read_more')}}                            </a>                        </p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="component-promo featured col-xs-12 col-sm-4">
                     <div class="row black">
                        <div class="col-xs-12">
                           <a href="/topaz" target="">
                              <div class="image-container">
                                 <img alt="Topaz" class=" img-responsive lazyloaded" src="img/gemstones/topaz.png">                            
                              </div>
                           </a>
                        </div>
                        <div class="col-xs-12 text-center">
                           <div class="component-promo-text">
                              <p class="slide-meta ">                                                                                </p>
                              <h4 class="title">swarovski GENUINE<br>TOPAZ</h4>
                              <div>
                                 <p><br></p>
                              </div>
                              <p class="related-link">                            <a href="/topaz" target="">                                {{trans('products.read_more')}}                            </a>                        </p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="component-promo featured col-xs-12 col-sm-4">
                     <div class="row black">
                        <div class="col-xs-12">
                           <a href="/black_spinel" target="">
                              <div class="image-container">
                                 <img alt="Black Spinel" class="lazyload img-responsive" src="img/gemstones/black_spinel.jpg">                            
                              </div>
                           </a>
                        </div>
                        <div class="col-xs-12 text-center">
                           <div class="component-promo-text">
                              <p class="slide-meta ">                                                                                </p>
                              <h4 class="title">swarovski GENUINE&nbsp;Black<br>spinel<br></h4>
                              <div>
                                 <p><br></p>
                              </div>
                              <p class="related-link">                            <a href="/black_spinel" target="">                                {{trans('products.read_more')}}                            </a>                        </p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="component-promo featured col-xs-12 col-sm-4">
                     <div class="row black">
                        <div class="col-xs-12">
                           <a href="/smoky_quartz" target="">
                              <div class="image-container">
                                 <img alt="Smoky Quartz" class="lazyload img-responsive" src="img/gemstones/Smoky_Quartz.png">                            
                              </div>
                           </a>
                        </div>
                        <div class="col-xs-12 text-center">
                           <div class="component-promo-text">
                              <p class="slide-meta ">                                                                                </p>
                              <h4 class="title">swarovski GENUINE smoky<br>quartz</h4>
                              <div>
                                 <p><br></p>
                              </div>
                              <p class="related-link">                            <a href="/smoky_quartz" target="">                                {{trans('products.read_more')}}                            </a>                        </p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="component-promo featured col-xs-12 col-sm-4">
                     <div class="row black">
                        <div class="col-xs-12">
                           <a href="/marcasite" target="">
                              <div class="image-container">
                                 <img alt="Shield Cut" class="lazyload img-responsive" src="img/gemstones/shield_cut.png">                            
                              </div>
                           </a>
                        </div>
                        <div class="col-xs-12 text-center">
                           <div class="component-promo-text">
                              <p class="slide-meta ">                                                                                </p>
                              <h4 class="title">swarovski <br>marcasite<br><br></h4>
                              <div>
                                 <p><br></p>
                              </div>
                              <p class="related-link">                            <a href="/marcasite" target="">                                {{trans('products.read_more')}}                            </a>                        </p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('scripts')
<script> </script>
@endsection