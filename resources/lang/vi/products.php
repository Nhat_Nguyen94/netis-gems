<?php 
return [
	'products_home'=>'Sản Phẩm',
	'our_company'=>'Về Netis Gems',
	'genuine_gemstones'=>'Đá Quý',
	'created_stones'=>'Đá nhân tạo',
	'about_swarovski'=>'Về SWAROVSKI',
	'history'=>'Lịch Sử',
	'news'=>'Tin tức',
	'contact_us'=>'Liên hệ',
	'search'=>'Tìm kiếm',
	'name'=>'Tên',
	'phone'=>'Số điện thoại',
	'address'=>'Địa chỉ',
	'business_development_manager'=>'Trưởng phòng phát triển kinh doanh',
	'account_manager'=>'Trưởng phòng quản lý khách hàng',
	'discover_more'=>'Tìm hiểu thêm',
	'read_more'=>'đọc thêm',
	'find_out_more'=>'tìm hiểu thêm',
	'back'=>'Quay lại',
	'language'=>'Ngôn Ngữ',
	'please_find_news'=>'Vui lòng xem bên dưới tin tức cuối cùng liên quan đến Đá quý và Đá quý Swarovski chính hãng',
	'please_find_video'=>'Vui lòng tìm các video mới nhất từ Swarovski Gemstones TM',
	'select_color'=>'Chọn màu sắc',
	'select_size'=>'Chọn kích thước'
];

 ?>